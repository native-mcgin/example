package uk.co.nativefinance;

import java.math.BigDecimal;

public class LoanProduct {

    private final Integer productId;
    private final Integer minRisk;
    private final Integer maxRisk;
    private final BigDecimal minInterestRate;
    private final BigDecimal maxInterestRate;

    private LoanProduct(final Builder builder) {
        productId = builder.productId;
        minRisk = builder.minRisk;
        maxRisk = builder.maxRisk;
        minInterestRate = builder.minInterestRate;
        maxInterestRate = builder.maxInterestRate;
    }

    public Integer getProductId() {
        return productId;
    }

    public Integer getMinRisk() {
        return minRisk;
    }

    public Integer getMaxRisk() {
        return maxRisk;
    }

    public BigDecimal getMinInterestRate() {
        return minInterestRate;
    }

    public BigDecimal getMaxInterestRate() {
        return maxInterestRate;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(final LoanProduct copy) {
        Builder builder = new Builder();
        builder.productId = copy.getProductId();
        builder.minRisk = copy.getMinRisk();
        builder.maxRisk = copy.getMaxRisk();
        builder.minInterestRate = copy.getMinInterestRate();
        builder.maxInterestRate = copy.getMaxInterestRate();
        return builder;
    }


    public static final class Builder {
        private Integer productId;
        private Integer minRisk;
        private Integer maxRisk;
        private BigDecimal minInterestRate;
        private BigDecimal maxInterestRate;

        private Builder() {
        }

        public Builder productId(final Integer val) {
            productId = val;
            return this;
        }

        public Builder minRisk(final Integer val) {
            minRisk = val;
            return this;
        }

        public Builder maxRisk(final Integer val) {
            maxRisk = val;
            return this;
        }

        public Builder minInterestRate(final BigDecimal val) {
            minInterestRate = val;
            return this;
        }

        public Builder maxInterestRate(final BigDecimal val) {
            maxInterestRate = val;
            return this;
        }

        public LoanProduct build() {
            return new LoanProduct(this);
        }
    }
}

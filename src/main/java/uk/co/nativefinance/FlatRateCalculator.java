package uk.co.nativefinance;

import java.math.BigDecimal;
import java.util.Optional;

public class FlatRateCalculator implements InterestRateCalculator {

    @Override
    public Optional<BigDecimal> calculateInterestRateForProject(final RealEstateProject project, final LoanProduct product) {
        return Optional.of(BigDecimal.TEN);
    }
}

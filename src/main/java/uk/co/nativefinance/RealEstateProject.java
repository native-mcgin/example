package uk.co.nativefinance;

public class RealEstateProject {
    private final Integer dealId;
    private final Integer riskRating;

    private RealEstateProject(final Builder builder) {
        dealId = builder.dealId;
        riskRating = builder.riskRating;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static Builder newBuilder(final RealEstateProject copy) {
        Builder builder = new Builder();
        builder.dealId = copy.getDealId();
        builder.riskRating = copy.getRiskRating();
        return builder;
    }

    public Integer getDealId() {
        return dealId;
    }

    public Integer getRiskRating() {
        return riskRating;
    }

    public static final class Builder {
        private Integer dealId;
        private Integer riskRating;

        private Builder() {
        }

        public Builder dealId(final Integer val) {
            dealId = val;
            return this;
        }

        public Builder riskRating(final Integer val) {
            riskRating = val;
            return this;
        }

        public RealEstateProject build() {
            return new RealEstateProject(this);
        }
    }
}

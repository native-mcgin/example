package uk.co.nativefinance;

import java.math.BigDecimal;
import java.util.Optional;

public interface InterestRateCalculator {
    Optional<BigDecimal> calculateInterestRateForProject(RealEstateProject project, LoanProduct product);
}

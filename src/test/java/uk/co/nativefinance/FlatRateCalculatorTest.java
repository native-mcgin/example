package uk.co.nativefinance;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class FlatRateCalculatorTest
{
    /**
     * Test to show how to use calculator and model
     */
    @Test
    public void brokenCalculatorShouldCalculateCorrectRate()
    {
        LoanProduct product = LoanProduct.newBuilder()
                .productId(1)
                .maxInterestRate(BigDecimal.valueOf(10))
                .minInterestRate(BigDecimal.valueOf(2))
                .maxRisk(35)
                .minRisk(10)
                .build();

        RealEstateProject project = RealEstateProject.newBuilder()
                .dealId(1)
                .riskRating(35)
                .build();

        assertTrue(new FlatRateCalculator().calculateInterestRateForProject(project, product).isPresent(), "Should calculate a price");
        assertTrue(new FlatRateCalculator().calculateInterestRateForProject(project, product).get().equals(BigDecimal.TEN), "Rate should be 10 percent");
    }
}

# Canditate Code Sample

## Task

Please create a new implementation of the InterestRateCalculator interface which calculates the interest rate for a given product/project combination.

The requirements for the calculator are as follows:

- If the risk of the RealEstateProject is outside the bounds of the LoanProduct then the calculator should not calculate an interest rate (it should return Optional.empty())
- If an interest rate can be calculated it should be within the range of minInterestRate and maxInterestRate 
- The calculated interest rate should increase linearly with the project risk
- Where the project risk is equal to the *minimum* risk of a product the calculated interest rate should be equal to the *minimum* interest rate
- Where the project risk is equal to the *maximum* risk of a product the calculated interest rate should be equal to the *maximum* interest rate
- The risk of a project can range between 1 (lowest risk) and 100 (highest risk)
- The interest rate should never be negative or above 20 percent

## General Information

The project has been set up to be compiled/run with JDK 15 although there is no requirement to use any specific features from that version.
If you have not already installed JDK 15 you can make use of the Dockerfile in the project by running:

```
docker build .
``` 

Which will build and execute the tests in a docker container.  The first run will be slow as it downloads all the maven artifacts, but subsequent runs will be fast unless you amend the pom file.

## Expectations

Please include a text file outlining any assumptions/decisions you make.
We will be looking to see if the code is understandable, tested and working.



FROM maven:3.6.3-openjdk-15 as mavenDeps

COPY pom.xml pom.xml

RUN mvn -B -f pom.xml clean org.apache.maven.plugins:maven-dependency-plugin:3.1.2:go-offline

COPY . .

RUN mvn -B clean install